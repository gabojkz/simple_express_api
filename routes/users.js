const express = require('express');
const router = express.Router();
const userService = require('../services/user_service');

/* GET users listing. */
router.get('/', function(req, res, next) 
{
  const users = userService.all();

  res.status(200).json(users);
});

/* POST new user */
router.post('/', function(req, res, next) 
{
  const email = req.body.email
  const emailFormat = /^[a-zA-Z0-9\W]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$/
  if (!email || !emailFormat.test(email)) return res.status(400).json({ error: 'invalid email address' })
  
  const givenName = req.body.givenName
  if (!givenName) return res.status(400).json({ error: 'invalid givenName' }) 

  const familyName = req.body.familyName
  if(!familyName) return res.status(400).json({ error: 'invalid familyName' })
   
  const user = userService.register({ email, givenName, familyName });
  
  return res.status(201).json(user);
});

/* GET user single */
router.get('/:id', function(req, res, next) 
{
  const id = req.params.id;
  if (!id) return res.status(400).json({ error: 'missing param id' })
  
  const user = userService.find(id);
  if (!user) return res.status(404).json({ error: 'user not found' })

  return res.status(200).json(user);  
});

/* DEL user */
router.delete('/:id', function(req, res, next) 
{
  const id = req.params.id;
  if (!id) return res.status(400).json({ error: 'missing param id' })

  const user = userService.find(id);  
  if (!user) return res.status(404).json({ error: 'user not found' })

  const success = userService.destroy(user);
  if (!success) return res.status(500).json({ error: 'could not remove user' })

  return res.sendStatus(204); 
});

/* PUT new user */
router.put('/:id', function(req, res, next) 
{
  const id = req.params.id
  if (!id) return res.status(400).json({ error: 'missing param id' })

  const email = req.body.email
  const emailFormat = /^[a-zA-Z0-9\W]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$/
  if (!email || !emailFormat.test(email)) return res.status(400).json({ error: 'invalid email address' })
  
  const givenName = req.body.givenName
  if (!givenName) return res.status(400).json({ error: 'invalid givenName' }) 

  const familyName = req.body.familyName
  if(!familyName) return res.status(400).json({ error: 'invalid familyName' })
  
  const user = userService.find(id)
  if (!user) return res.status(404).json({ error: 'user not found' })

  const success = userService.update(user, { email, givenName, familyName });
  if (!success) return res.status(500).json({ error: 'could not update user' }) 
  
  return res.sendStatus(204);
});


module.exports = router;
