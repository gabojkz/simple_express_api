/**
* In memory database for storing basic records
*/
class DB
{
  constructor()
  {
    this._records = {};
    this._lastId = 1;
  }

  all()
  {
    return Object.values(this._records);
  }

  create(data)
  {
    const nextId = this.nextId();
    const record = { id: nextId, ...data};
   
    this._records[nextId] = record; 

    return record;
  }

  find(id)
  {
    return this._records[id];
  }

  update(id, userData)
  {
    this._records[id] = Object.assign({}, {
      id: id
    }, userData);

    return this._records[id];
  }

  destroy(id)
  {
    delete this._records[id];
    return this.count();
  }

  count() 
  {
    return Object.keys(this._records).length;
  }

  nextId()
  {
    if (!this.count()) return this._lastId; 
    
    this._lastId += 1;

    return this._lastId;
  }
};

module.exports = DB;
