class User 
{
  constructor(data)
  {
    this.id = data.id
    this.email = data.email
    this.givenName = data.givenName
    this.familyName = data.familyName
  }
}

module.exports = User;
