# API written with express

no-persistence in memory database

---

## REST Routes

  - /users
  - GET list users
  - GET(:id) show single user
  - POST create user
  - PUT(:id) update user
  - DEL(:id) delete user

---

## Perl client tool

Help to test the routes.

```bash
Usage: ./client -h 
```