const User = require('../models/user')
const DB = require('../databases/local_memory')
const db = new DB()

const all = () => 
{
  const usersData = db.all();
  
  return usersData.map( user => new User(user) );
};

const register = (userData) => 
{
  const createdUser = db.create(userData)
  return new User(createdUser);
}

const find = (id) => 
{
  const userData = db.find(id)
  if (!userData) return null;

  return new User(userData)
}

const destroy = (user) => 
{
  const oldTotal = db.count()
  const newTotal = db.destroy(user.id)
  
  return oldTotal > newTotal;
};

const update = (user, userData) =>
{
  const userUpdatedData = db.update(user.id,  userData)
  if(!userUpdatedData) return null;

  return new User(userUpdatedData);
};

module.exports = { all, register, find, destroy, update };
