const chai = require('chai')
const sinon = require('sinon')
const assert = chai.assert
const expect = chai.expect

const userService = require('../../services/user_service')
const UserModel = require('../../models/user')
const MemoryDB = require('../../databases/local_memory')

const userFake = [{
  id: 1,
  givenName: 'joe',
  familyName: 'Alan',
  email: 'joe@alan.com',
}]

describe('User service', function() {
  it('should fetch all users', function() {
    sinon.stub(MemoryDB.prototype, 'all').returns(userFake);
    
    const users = userService.all()
    
    assert.isArray(users, 'is not an array')
    assert.instanceOf(users[0], UserModel)
    expect(users).to.have.deep.members(userFake);
  })
})